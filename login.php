<?php require "bootstrap.php";
if (isset($_SESSION['login_status']) && $_SESSION['login_status'] == 1) {
	redirect('/index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="#">
  <meta name="author" content="JustPage.in">
  <meta name="keyword" content="Web Designer & Web Development">
  <link rel="shortcut icon" href="<?php echo $config['site_url']; ?>/img/favicon.png">

  <title><?php echo $config['site_name']; ?></title>

  <link href="<?php echo $config['site_url']; ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $config['site_url']; ?>/css/bootstrap-reset.css" rel="stylesheet">
  <link href="<?php echo $config['site_url']; ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="<?php echo $config['site_url']; ?>/assets/toastr-master/toastr.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $config['site_url']; ?>/css/style.css" rel="stylesheet">
  <link href="<?php echo $config['site_url']; ?>/css/style-responsive.css" rel="stylesheet" />
</head>
<body class="login-body">
  <div class="container">
    <form class="form-signin" action="<?php echo $config['form_action_url'] ?>/do_login.php" method="post">
      <h2 class="form-signin-heading">sign in now</h2>
      <div class="login-wrap">
        <input type="text" class="form-control" placeholder="User ID" name="email">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
      </div>
    </form>
  </div>
  <script src="<?php echo $config['site_url']; ?>/js/jquery.js"></script>
  <script src="<?php echo $config['site_url']; ?>/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo $config['site_url'];?>/assets/toastr-master/toastr.js"></script>
</body>

<script type="text/javascript">
  <?php if (isset($_SESSION['flash'])) {?>
    var type = "<?php echo (isset($_SESSION['flash']['type'])) ? $_SESSION['flash']['type'] : 'info' ?>";
    switch(type){
			case 'info':
					toastr.info("<?php echo $_SESSION['flash']['message'] ?>");
					break;
			case 'warning':
					toastr.warning("<?php echo $_SESSION['flash']['message'] ?>");
					break;
			case 'success':
					toastr.success("<?php echo $_SESSION['flash']['message'] ?>");
					break;
			case 'error':
					toastr.error("<?php echo $_SESSION['flash']['message'] ?>");
					break;
    }
  <?php unset($_SESSION['flash']); 
	} ?>
</script>
</html>