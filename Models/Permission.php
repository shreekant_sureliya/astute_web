<?php

//namespace Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Permission extends Eloquent
{
    protected $table = 'permission';
    protected $primaryKey = 'id';
	protected $fillable = [ 'user_id', 'permission', 'is_active' ];
	
    public function __construct($table_name = null){
        parent::__construct();
        if($table_name != NULL){
          $this->table = $table_name;
        }
    }

   /*
   public function todo()
   {
      return $this->hasMany('Todo');
   }
   */

 }

 ?>