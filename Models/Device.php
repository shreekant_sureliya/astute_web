<?php

//namespace Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Device extends Eloquent
{
    protected $table = 'devices';
    protected $primaryKey = 'id';

    public function __construct($table_name = null){
        parent::__construct();
        if($table_name != NULL){
          $this->table = $table_name;
        }
    }

   
   public function user()
   {
      return $this->belongsTo(User::class,'user_id','user_id');
   }

 }

 ?>