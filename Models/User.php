<?php

//namespace Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';

    public function __construct($table_name = null){
        parent::__construct();
        if($table_name != NULL){
          $this->table = $table_name;
        }
    }

   /*
   public function todo()
   {
      return $this->hasMany('Todo');
   }
   */

 }

 ?>