<?php

require "../bootstrap.php";
use Carbon\Carbon;

$first_name = get_form_value('first_name');
$last_name = get_form_value('last_name');
$email = get_form_value('email');
$password = get_form_value('password');
$contact_no = get_form_value('contact_no');
$module = get_form_value('module');
$expiry_date = get_form_value('expiry_date');

include('validation/user_validate.php');

$user = new User;
$user->first_name = $first_name;
$user->last_name = $last_name;
$user->email = $email;
$user->password = md5($password);
$user->contact_no = $contact_no;
$user->user_type = 'admin';
$user->save();

/*foreach($module as $m){

		$mm = Module::findorfail($m);
		$Subscription = new Subscription;
		$Subscription->user_id = $user->user_id;
		$Subscription->module_id = $mm->module_id;
		$current_time = Carbon::now();
		$Subscription->register_date = $current_time->toDateTimeString();
		if($expiry_date == NULL){
			$expiry_time = $current_time->add($mm->module_days, 'day');
			$Subscription->expiry_date = $expiry_time->toDateTimeString();
		}else{
			$expiry_time = Carbon::createFromFormat('m/d/Y', $expiry_date);
			$Subscription->expiry_date = $expiry_time->toDateTimeString();
		}
		$Subscription->save();

}*/

FlashMessage::set('User Add Successfully','success');
redirect('/index.php?view=master_users')

?>