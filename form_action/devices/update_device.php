<?php

require "../../bootstrap.php";
use Carbon\Carbon;
$id = get_form_value('id');
$device_id = get_form_value('device_id');
$device_id=strtoupper($device_id);
$url = get_form_value('url');
$user_id = get_form_value('user_id');
$gmail = get_form_value('gmail');
$password = get_form_value('password');
$sensor_id = get_form_value('sensor_id');
$file = $_FILES["file"]["name"];

if(isset($device_id) && isset($url) && isset($user_id) && isset($id))
{
	if(Device::where('device_id',$device_id)->where('id','!=',$id)->first()){

		FlashMessage::set('This Device Id Already Exists','error');
		redirect('/index.php?view=devices&action=edit&id='.$id.'');

	}else{
		$device = Device::find($id);
		$device->device_id = $device_id;
		$device->user_id = $user_id;
		$device->url = $url;
		$device->gmail = $gmail;
		$device->password = $password;
		$device->sensor = json_encode($sensor_id);
		if(isset($file) && $file!=null){
			$device->json_file = $file;
		}
		$device->save();

		$target_dir = $main_root_path."/firebase_json/";
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)){
			
		}else{
			FlashMessage::set('Some Problem in Uploded file','success');
			redirect('/index.php?view=devices');
		}

		FlashMessage::set('Device Update Successfully','success');
		redirect('/index.php?view=devices');
	}

}

FlashMessage::set('Some Problem in Input Feild','error');
redirect('/index.php?view=devices&action=edit&id='.$id.'');

?>