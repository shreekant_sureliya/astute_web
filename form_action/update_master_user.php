<?php

require "../bootstrap.php";
use Carbon\Carbon;

$user_id = get_form_value('user_id');
$first_name = get_form_value('first_name');
$last_name = get_form_value('last_name');
$email = get_form_value('email');
$contact_no = get_form_value('contact_no');
$module = get_form_value('module');
$expiry_date = get_form_value('expiry_date');

include('validation/user_validate.php');

$user = User::find($user_id);
$user->first_name = $first_name;
$user->last_name = $last_name;
$user->email = $email;
$user->contact_no = $contact_no;
$user->user_type = 'admin';
$user->save();

FlashMessage::set('User Update Successfully','success');
redirect('/index.php?view=master_users')

?>