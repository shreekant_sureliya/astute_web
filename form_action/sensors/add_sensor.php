<?php

require "../../bootstrap.php";
use Carbon\Carbon;

$name = get_form_value('name');


if(isset($name))
{
	if(Sensor::where('name',$name)->first()){

		FlashMessage::set('This Name Already Exists','error');
		redirect('/index.php?view=sensors&action=add');

	}else{
		$sensor = new Sensor;
		$sensor->name = $name;
		$sensor->save();

		FlashMessage::set('Sensor Add Successfully','success');
		redirect('/index.php?view=sensors');
	}

}

FlashMessage::set('Some Problem in Input Feild','error');
redirect('/index.php?view=sensors&action=add');

?>