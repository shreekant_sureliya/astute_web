<?php

require "../../bootstrap.php";
use Carbon\Carbon;
$id = get_form_value('id');
$name = get_form_value('name');


if(isset($name) && isset($id))
{
	if(Sensor::where('id','!=',$id)->where('name',$name)->first()){

		FlashMessage::set('This Name Already Exists','error');
		redirect('/index.php?view=sensors&action=update&id='.$id.'');

	}else{
		$sensor = Sensor::find($id);
		$sensor->name = $name;
		$sensor->save();

		FlashMessage::set('Sensor Update Successfully','success');
		redirect('/index.php?view=sensors');
	}

}

FlashMessage::set('Some Problem in Input Feild','error');
redirect('/index.php?view=sensors&action=update&id='.$id.'');

?>