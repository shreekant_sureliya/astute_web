<?php

require "../bootstrap.php";
use Carbon\Carbon;

$user_id = get_form_value('user_id');
$user_name = get_form_value('user_name');
$email = get_form_value('email');
$contact_no = get_form_value('contact_no');
$permission = get_form_value('permission');

include('validation/user_update_validate.php');

$user = User::find($user_id);
$user->user_name = $user_name;
$user->email = $email;
$user->contact_no = $contact_no;
$user->save();

Permission::where(array('user_id'=>$user_id))->update(['is_active' => 0]);
foreach($permission as $p){
	Permission::updateOrCreate(array(
		'user_id'=>$user_id,
		'permission'=>$p
	),array(
		'user_id'=>$user_id,
		'permission'=>$p,
		'is_active'=>1
	));
}

FlashMessage::set('User Update Successfully','success');
redirect('/index.php?view=users')

?>