<?php

require "../bootstrap.php";

$email = get_form_value('email');
$password = get_form_value('password');

$User = new User;
$user_data = $User->where(array('email'=>$email,'password'=>$password,'is_active'=>1))->first();

if($user_data == NULL){

		$user_data = $User->where(array('email'=>$email))->first();
		if($user_data == NULL){
				FlashMessage::set($email.' is not registered.','error');
		}else if($user_data->is_active == 0){
				FlashMessage::set($email.' is blocked by admin.','warning');
		}else{
				FlashMessage::set('Password is invalid, please check that.','warning');
		}

		redirect('/login.php');

}else{

		// do login
		$user_data = $user_data->toArray();
		$_SESSION['login_status'] = '1';
		$_SESSION['user_data'] = $user_data;
		
		FlashMessage::set('Welcome '.$user_data['user_name'],'success');
		redirect('/index.php');

}


?>