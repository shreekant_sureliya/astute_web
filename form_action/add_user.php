<?php

require "../bootstrap.php";
use Carbon\Carbon;

$user_name = get_form_value('user_name');
$email = get_form_value('email');
$password = get_form_value('password');
$contact_no = get_form_value('contact_no');
$permission = get_form_value('permission');
include('validation/user_validate.php');

$user = new User;
$user->user_name = $user_name;
$user->email = $email;
$user->password = $password;
$user->contact_no = $contact_no;
$user->user_type = 'user';
$user->save();

foreach($permission as $p){

		$permission = new Permission;
		$permission->user_id = $user->user_id;
		$permission->permission = $p;
		$permission->save();

}

FlashMessage::set('User Add Successfully','success');
redirect('/index.php?view=users')

?>