-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2022 at 08:13 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `astute`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `json_file` varchar(255) NOT NULL,
  `gmail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `sensor` text NOT NULL,
  `last_update` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `device_id`, `user_id`, `url`, `json_file`, `gmail`, `password`, `sensor`, `last_update`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'AST_001', 3, 'https://esp32-ee967.firebaseio.com', 'esp32-ee967-firebase-adminsdk-ydox6-59c5c497dc.json', 'sixthsensegecr@gmail.com', 'weare@2020', '[\"1\",\"3\"]', '1650994954', 1, '2022-03-27 12:27:33', '2022-04-26 14:12:35'),
(2, 'AST_002', 3, 'https://try123-aba79-default-rtdb.firebaseio.com', 'try123-aba79-firebase-adminsdk-vkf8v-c254cf7266.json', 'new@gmail.com', '123', '[\"1\",\"3\"]', NULL, 1, '2022-03-29 17:22:22', '2022-03-29 17:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `user_id`, `permission`, `is_active`, `created_at`, `updated_at`) VALUES
(6, 3, 'devices', 1, '2022-02-20 04:39:30', '2022-02-20 06:01:05'),
(7, 1, 'devices', 1, '2022-02-20 04:40:44', '2022-02-20 04:40:44'),
(8, 2, 'users', 1, NULL, NULL),
(9, 2, 'devices', 1, NULL, NULL),
(10, 3, 'user_dashboard', 1, '2022-02-20 05:59:19', '2022-02-20 06:01:05'),
(11, 3, 'data', 1, '2022-02-20 06:01:05', '2022-02-20 06:01:05'),
(12, 2, 'dashboard', 1, NULL, NULL),
(13, 2, 'sensors', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sensors`
--

CREATE TABLE `sensors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sensors`
--

INSERT INTO `sensors` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'V', 1, '2022-03-27 06:51:57', '2022-03-27 17:36:14'),
(3, 'I', 1, '2022-03-27 06:54:39', '2022-03-27 17:36:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `email`, `password`, `contact_no`, `user_type`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'test', 'hi@gmail.com', '123', '1234567890', 'user', 1, '2022-02-05 19:31:00', '2022-02-05 19:40:04'),
(2, 'admin', 'admin@gmail.com', '123', '', 'admin', 1, '2022-02-13 04:26:26', '2022-02-13 04:26:26'),
(3, 'ast', 'ast@gmail.com', '123', '1234567890', 'user', 1, '2022-02-13 05:18:49', '2022-02-13 05:18:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
