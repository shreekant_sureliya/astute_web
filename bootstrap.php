<?php
error_reporting(-1);
ob_start();
@session_start();

date_default_timezone_set('Asia/Kolkata');

require "vendor/autoload.php";

$config                    = array();
$config['site_url']        = 'http://localhost/astute_web';
$config['site_name']       = 'Astute';
$config['form_action_url'] = $config['site_url'] . '/form_action';
$config['ajax_url']        = $config['site_url'] . '/ajax/ajax.php';

$main_root_path = $_SERVER['DOCUMENT_ROOT'] . '/astute_web/';

global $user_name, $user_email, $user_contact_no, $user_type, $parent_admin_user_id;
include_once $main_root_path . "/class/Auth.php";

if (!isset($db_mode)) {
  if (!preg_match('/login\.php/', $_SERVER['REQUEST_URI'])) {
    Auth::check_login();
  }
}

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
  "driver"   => "mysql",
  "host"     => "127.0.0.1",
  "database" => "astute",
  "username" => "root",
  "password" => "",
]);

/*$capsule->addConnection([
"driver" => "mysql",
"host" =>"127.0.0.1",
"database" => "jpsales_asian",
"username" => "jpsales_asian",
"password" => "IzB%Dq0*=TLv"
]); */

//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
//$capsule->bootEloquent();

$all_permission_list = array('devices','users','user_dashboard','dashboard','data','sensors');

include_once $main_root_path . "/Functions/site_functions.php";
include_once $main_root_path . "/class/Crontab.php";
include_once $main_root_path . "/class/SidebarMenu.php";
include_once $main_root_path . "/class/FlashMessage.php";
