<?php

class Auth{
    
    /*
	public function __construct(){
       if($this->is_session_started() === FALSE){ session_start(); }
    }*/

    public function is_session_started(){
        if(version_compare(phpversion(), '5.4.0', '>=')){
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            return session_id() === '' ? FALSE : TRUE;
        }
    }

    public static function check_login(){
        
        global $config;
        if(!(isset($_SESSION['login_status']) && $_SESSION['login_status'] != '')){
            header('Location:'.$config['site_url'].'/login.php');
        }else{
          Auth::set_user_data();
        }

    }

    public static function set_user_data(){
      
        global $user_name,$user_email,$user_contact_no,$user_type,$parent_admin_user_id;
        $user_name = $_SESSION['user_data']['user_name'];
        $user_email = $_SESSION['user_data']['email'];
        $user_contact_no = $_SESSION['user_data']['contact_no'];
        $user_type = $_SESSION['user_data']['user_type'];

    }

    public static function logout(){
        global $config;
        session_destroy();
        header('Location:'.$config['site_url'].'/login.php');

    }

}

?>