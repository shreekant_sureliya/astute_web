<?php

class FlashMessage{

	 public function __construct(){
        if(!isset($_SESSION)){ 
            session_start(); 
        } 
    }

    public static function set($message, $type = 'info'){
        $_SESSION['flash'] = array(
            'message' => $message,
            'type'    => $type
        );
    }

    public static function set_also($message, $type = 'info'){
        $_SESSION['flash'][] = array(
            'message' => $message,
            'type'    => $type
        );
    }

}

?>