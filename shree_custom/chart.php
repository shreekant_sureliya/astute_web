<!DOCTYPE html>
<html>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>

<body>
<canvas id="myChart" style="width:100%;max-width:600px"></canvas>  
</body>


<script>

var xValues = [50,60,70,80,90,100,110,50,50,140,150];
var yValues = [7,8,8,9,9,9,10,11,5,14,15];
new Chart("myChart", {
  type: "line",
  
  data: {
    labels: xValues,
    datasets: [{
      fill: true,
      lineTension: 0,
      backgroundColor: "rgba(54, 162, 235,0.5)",
      borderColor: "rgba(0,0,255,0.4)",
      data: yValues
    }]
  },
  options: {
  
    
    legend: {display: false},
    grid:false,
    
  }
});
</script>

</html>


