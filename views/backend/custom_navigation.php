<header class="header dark-bg">
  <div class="sidebar-toggle-box"> <i class="fa fa-bars"> </i> </div>
  <a class="logo" href="javascript:;"> <?php echo $config['site_name']; ?> </a>
  <?php if(!isset($simple_display_mode) || !$simple_display_mode){?>
  <div class="top-nav ">
    <ul class="nav pull-right top-menu">
      <!-- <li><h5 class="crnt_yr"> Current Year : 2019-20 </h5></li> -->
      <li class="dropdown"> 
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
					<img alt="" src="<?php echo $config['site_url'];?>/img/avatar1_small.jpg" /> 
					<span class="username"> <?php echo ucwords($user_name); ?> </span> 
					<b class="caret"> </b>
				</a>
        <ul class="dropdown-menu extended logout dropdown-menu-right">
          <div class="log-arrow-up"> </div>
          <li> <a href="#"> <i class=" fa fa-suitcase"> </i> Profile </a> </li>
          <li> <a href="#"> <i class="fa fa-cog"> </i> Settings </a> </li>
          <li> <a href="#"> <i class="fa fa-bell-o"> </i> Notification </a> </li>
          <li><a href="<?php echo $config['site_url']; ?>/logout.php" onclick="return confirm('Are you sure to logout?')"><i class="fa fa-key"></i> <span>Log Out</span></a></li>
        </ul>
      </li>
			<li class="sb-toggle-rights"><a href="<?php echo $config['site_url']; ?>/logout.php" onclick="return confirm('Are you sure to logout?')" style="padding: 8px;"><i class="fa fa-sign-out"></i></a>
			</li>
    </ul>
  </div>
  <?php }?>
</header>