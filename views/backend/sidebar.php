<?php include_once('sidebar_menu/'.$user_type.'.php');
//dd($data['side_ui_menu']);
?>
<ul class="sidebar-menu" id="nav-accordion">
	<?php if(isset($data['side_ui_menu']) && count($data['side_ui_menu']) > 0 && $data['side_ui_menu'] != ''){
		foreach($data['side_ui_menu'] as $menu){
			if(isset($menu['sub_menu']) && $menu['sub_menu'] != ''){ 
			?>
				<li class="sub-menu"> 
					<a href="javascript:;" class="<?php if($data['page'] == $menu['page']){ echo "active"; } ?>"> 
						<i class="<?php echo $menu['icon']; ?>"> </i> 
						<span> <?php echo $menu['text']; ?> </span> 
					</a>
					<?php if(!empty($menu['sub_menu'])){?>
					<ul class="sub">
						<?php foreach($menu['sub_menu'] as $sub_menu){ ?>
						<li <?php if(isset($data['sub_page']) && ($data['sub_page'] == $sub_menu['page'])){ echo 'class="active"'; } ?> >
							<a href="<?php echo $sub_menu['route']; ?>" class="<?php echo $sub_menu['class'] ?>" id="<?php echo $sub_menu['id'] ?>"> 
								<i class="<?php echo $menu['icon']; ?>"></i> 
								<span><?php echo $menu['text']; ?></span>
							</a>
						</li>
						<?php } ?>
					</ul>
					<?php }?>
				</li>
				<?php }else{
				if($data['page'] == $menu['page']){ 
					$cls = 'active ' . $menu['class']; 
				}else{
					$cls = $menu['class']; 
				}
				?>
				<li> <a class="<?php echo $cls;?>" id="<?php echo $menu['id'] ?>" href="<?php echo $menu['route'] ?>"> <i class="<?php echo $menu['icon'] ?>"> </i> <span> <?php echo $menu['text'] ?> </span> </a> </li>
		<?php } 
		} 
	}?>
</ul>