<?php
require "bootstrap.php";
include 'view/' . $view_folder . '/content_head.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="#" name="description"/>
<meta content="astute.in" name="author"/>
<meta content="Web Designer & Web Development" name="keyword"/>
<link href="<?php echo $config['site_url']; ?>/img/favicon.png" rel="shortcut icon"/>
<title><?php echo $config['site_name']; ?></title>
<!-- Bootstrap core CSS -->

<link href="<?php echo $config['site_url']; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $config['site_url']; ?>/css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link href="<?php echo $config['site_url']; ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="<?php echo $config['site_url']; ?>/assets/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $config['site_url']; ?>/assets/bootstrap-daterangepicker/daterangepicker.css" />
    
<link href="<?php echo $config['site_url']; ?>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" media="screen" rel="stylesheet" type="text/css"/>
<link href="<?php echo $config['site_url']; ?>/css/owl.carousel.css" rel="stylesheet" type="text/css">
<!--right slidebar-->
<link href="<?php echo $config['site_url']; ?>/css/slidebars.css" rel="stylesheet">
<link href="<?php echo $config['site_url']; ?>/assets/toastr-master/toastr.css" rel="stylesheet" type="text/css" />
<!-- Custom styles for this template -->
<link href="<?php echo $config['site_url']; ?>/css/style.css" rel="stylesheet">
<link href="<?php echo $config['site_url']; ?>/css/style-responsive.css" rel="stylesheet"/>
</head>
</html>
<body class="">
<section id="container">
  <?php 
	if(!isset($simple_display_mode) || !$simple_display_mode){
		include('custom_navigation.php');
	}
	?>
	<?php if(!isset($simple_display_mode) || !$simple_display_mode){ ?>
  <aside><div class="nav-collapse " id="sidebar"><?php include('sidebar.php'); ?></div></aside>
	<?php } ?>
  <section id="main-content">
    <section class="wrapper">
      <?php include('view/'.$view_folder.'/content_header.php'); ?>
     	<?php include('view/'.$view_folder.'/content.php'); ?>
    </section>
  </section>
  <footer class="site-footer">
    <div class="text-right" style="width: 96%;margin: 0 auto;"> Developed By <a href="http://astute.in" target="_blank"> Astute </a>
      <!-- <a class="go-top text-center" href="#"><i class="fa fa-angle-up"></i></a> -->
    </div>
  </footer>
</section>
<script src="<?php echo $config['site_url'];?>/js/jquery.js"></script>
<script src="<?php echo $config['site_url'];?>/js/bootstrap.bundle.min.js"></script>
<script class="include" src="<?php echo $config['site_url'];?>/js/jquery.dcjqaccordion.2.7.js" type="text/javascript"></script>
<script src="<?php echo $config['site_url'];?>/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $config['site_url'];?>/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo $config['site_url'];?>/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo $config['site_url'];?>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="<?php echo $config['site_url'];?>/js/owl.carousel.js"></script>
<script src="<?php echo $config['site_url'];?>/js/jquery.customSelect.min.js"></script>
<script src="<?php echo $config['site_url'];?>/js/respond.min.js"></script>
<script type="text/javascript" src="<?php echo $config['site_url'];?>/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<?php echo $config['site_url'];?>/assets/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $config['site_url'];?>/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>

$('.datepicker_range').daterangepicker({
  locale: {
      format: 'DD/MM/YYYY'
    }
});
</script>
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="<?php echo $config['site_url'];?>/assets/Table/csvExport.js"></script>


<script src="<?php echo $config['site_url'];?>/assets/table/table2csv.js"></script>

<!--right slidebar-->
<script src="<?php echo $config['site_url'];?>/js/slidebars.min.js"></script>
<script src="<?php echo $config['site_url'];?>/assets/toastr-master/toastr.js"></script>
<script src="<?php echo $config['site_url'];?>/js/pickers/init-date-picker.js"></script>


  
<!--common script for all pages-->
<script src="<?php echo $config['site_url'];?>/js/common-scripts.js"></script>
<!--script for this page-->
<script src="<?php echo $config['site_url'];?>/js/sparkline-chart.js"></script>
<script src="<?php echo $config['site_url'];?>/js/easy-pie-chart.js"></script>
<script src="<?php echo $config['site_url'];?>/js/count.js"></script>
<?php include('view/'.$view_folder.'/content_footer.php'); ?>
<script type="text/javascript">
<?php if(isset($_SESSION['flash'])){
	if(isset($_SESSION['flash']['type'])){  ?>
	var type = "<?php echo (isset($_SESSION['flash']['type'])) ? $_SESSION['flash']['type'] : 'info' ?>";
	switch(type){
		case 'info':
			toastr.info("<?php echo $_SESSION['flash']['message'] ?>");
			break;
		case 'warning':
			toastr.warning("<?php echo $_SESSION['flash']['message'] ?>");
			break;
		case 'success':
			toastr.success("<?php echo $_SESSION['flash']['message'] ?>");
			break;
		case 'error':
			toastr.error("<?php echo $_SESSION['flash']['message'] ?>");
			break;
	}
	<?php }else{
		if(is_array($_SESSION['flash']) && count($_SESSION['flash']) > 0 && $_SESSION['flash'] != ''){
			foreach($_SESSION['flash'] as $flash){ 
				$rno = rand(123,999); ?>

				var type_<?php echo $rno; ?> = "<?php echo (isset($flash['type'])) ? $flash['type'] : 'info'; ?>";
				switch(type_<?php echo $rno; ?>){
					case 'info':
						toastr.info("<?php echo $flash['message'] ?>");
						break;
					case 'warning':
						toastr.warning("<?php echo $flash['message'] ?>");
						break;
					case 'success':
						toastr.success("<?php echo $flash['message'] ?>");
						break;
					case 'error':
						toastr.error("<?php echo $flash['message'] ?>");
						break;
				}
			<?php }
		}
	}
	unset($_SESSION['flash']); 
}?>
</script>
</body>