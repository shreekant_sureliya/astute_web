
<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
      <header class="card-header"> Dash Board </header>

      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>

      <h1 style="padding-left: 18%;">AST0005</h1>
      <canvas id="myChart" style="width:100%;max-width:600px"></canvas>  
      
      <script>
var xValues = [50,60,70,80,90,100,110,50,50,140,150];
var yValues = [7,8,8,9,14,9,12,11,8,14,15];
new Chart("myChart", {
  type: "line",
  
  data: {
    labels: xValues,
    datasets: [{
      fill: true,
      lineTension: 0,
      backgroundColor: "rgba(54, 162, 235,0.5)",
      borderColor: "rgba(0,0,255,0.4)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    grid:false,
    
  }
});
</script>


      

      <div class="card-body"> </div>
    </section>
  </div>
</div>
