<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>
  <style id="compiled-css" type="text/css">

.chartWrapper > canvas {
  position: absolute;
  left: 0;
  top: 0;
  pointer-events: none;
}
.chartAreaWrapper {
  width: 600px;
  /* overflow-x: scroll; */
}
<?php

$user_id = $_SESSION['user_data']['user_id'];
$device = Device::where('user_id',$user_id)->get();
$device_count = $device->count();
?>

  </style>
</head>
<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
      <header class="card-header"> User Dash Board </header>

<html>
<body>
<div class="row p-5">
  <?php 
  for($i=1;$i<=$device_count;$i++){?>
    <div class="col-8">
      <div class="chartWrapper">
      <div class="chartAreaWrapper">
          <canvas id="barChart_i<?php echo $i;?>" height="600" width="1200"></canvas>
      </div>
    </div>
  </div>
  <div class="col-4 text-center"> 
    <div class="card shadow-lg p-3 mb-5 bg-white rounded w-50">
    <div class="card-body">
      <h3 id="last_val_i<?php echo $i?>">0</h3>
    </div>
  </div>
    </div>

    <div class="col-8">
      <div class="chartWrapper">
      <div class="chartAreaWrapper">
          <canvas id="barChart_v<?php echo $i;?>" height="600" width="1200"></canvas>
      </div>
    </div>
  </div>
  <div class="col-4 text-center"> 
    <div class="card shadow-lg p-3 mb-5 bg-white rounded w-50">
    <div class="card-body">
      <h3 id="last_val_v<?php echo $i?>">0</h3>
    </div>
  </div>
    </div>
  <?php }
  ?>
</div>
</body>
</html>



<script type='text/javascript'>
// var canvas = document.getElementById("barChart");
// var canvas1 = document.getElementById("barChart1");
// var ctx = canvas.getContext('2d');
// var ctx1 = canvas1.getContext('2d');
// var chartType = 'line';
 var myBarChart_i = [];
 var myBarChart_v = [];
// var myBarChart1;
//var myBarChart=[];
// var ctx=[];

var formData = {action:'get_graph',no:9}; //Array 
  $.ajax({
      url : "<?php echo $config['ajax_url'] ?>",
      type: "POST",
      data : formData,
      success: function(data, textStatus, jqXHR)
      {
        data = JSON.parse(data);
        if(data.type == 'success'){
          //$('#last_val').html(data.last_val);
          //generate_graph(data.xvalue,data.yvalue,data.max,data.min);
          //console.log(data.xvalue);
          //console.log(data.xvalue_1);

          for(var i=1;i<= <?php echo $device_count; ?>;i++){
            var canvasi = document.getElementById("barChart_i"+i);
            var canvasv = document.getElementById("barChart_v"+i);
            var ctxi = canvasi.getContext('2d');
            var ctxv = canvasv.getContext('2d');
            var chartType = 'line';
            //console.log(data.xvalue[i]);
            xv= data.xvalue[i];
            yvi=data.yvalues_I[i];
            yvv=data.yvalues_V[i];

            var di = {
              labels:xv,
              datasets: [{
                label: "Device "+i,
                fill: true,
                lineTension: 0.5,
                backgroundColor: "#e0ecfc",
                borderColor: "#1f3bb3", // The main line color
                borderCapStyle: 'square',
                pointBorderColor: "white",
                pointBackgroundColor: "#1f3bb3",
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBackgroundColor: "#1f3bb3",
                pointHoverBorderColor: "#e0ecfc",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                data: yvi,
                spanGaps: true,
              }],
            }

            var dv = {
              labels:xv,
              datasets: [{
                label: "Device "+i,
                fill: true,
                lineTension: 0.5,
                backgroundColor: "green",
                borderColor: "green", // The main line color
                borderCapStyle: 'square',
                pointBorderColor: "white",
                pointBackgroundColor: "green",
                pointBorderWidth: 1,
                pointHoverRadius: 8,
                pointHoverBackgroundColor: "green",
                pointHoverBorderColor: "green",
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                data: yvv,
                spanGaps: true,
              }],
            }

              //myBarChart = myBarChart[i];
              //ctx = ctx[i];
              myBarChart_i[i] = new Chart(ctxi, {
                type: chartType,
                data: di,
                options: options
              });

              myBarChart_v[i] = new Chart(ctxv, {
                type: chartType,
                data: dv,
                options: options
              });
          }
        }
      },
  });

var options = {
  scales: {
       xAxes: [{
         ticks: {
           fontSize: 12,
           display: true,
           max:100,
           min:10,
         },
         gridLines: {
                display:false
          }     
       }],
       yAxes: [{
         ticks: {
           fontSize: 12,
           display: true,
         }, 
       }]
     },
  title: {
    fontSize: 18,
    display: true,
    position: 'bottom'
  }
};


// function init(d) {
//   // Chart declaration:
//   myBarChart = new Chart(ctx, {
//     type: chartType,
//     data: d,
//     options: options
//   });

//   myBarChart1 = new Chart(ctx1, {
//     type: chartType,
//     data: d,
//     options: options
//   });

// }

$(document).ajaxComplete(function () {
  setInterval(function() {
    get_data();
  }, 5000);
});

function get_data(){
  var formData = {action:'get_graph',no:1}; //Array 
  $.ajax({
      url : "<?php echo $config['ajax_url'] ?>",
      type: "POST",
      data : formData,
      success: function(d, textStatus, jqXHR)
      {
        chart_data = JSON.parse(d);
        if(chart_data.type == 'success'){
          for(var i=1;i<= <?php echo $device_count; ?>;i++){
            //console.log(chart_data.yvalue[i]);
            //myBarChart = myBarChart[i];
            //console.log(myBarChart.data.labels);
            // removeData();
            // pushData(chart_data.xvalue[i],chart_data.yvalue[i]);
            //console.log(chart_data.xvalue[i]);
            //myBarChart = myBarChart+i
           // console.log(myBarChart.data.labels);
            //myBarChart = myBarChart+i;
            //var yVal = 15
            //yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
            
            myBarChart_v[i].data.labels.splice(0, 1);
            myBarChart_v[i].data.datasets[0].data.splice(0, 1);

            myBarChart_v[i].data.labels.push(chart_data.xvalue[i]);
            myBarChart_v[i].data.datasets[0].data.push(parseFloat(chart_data.yvalues_V[i]));
            $('#last_val_v'+i).html(parseFloat(chart_data.yvalues_V[i])).fadeIn(100000);
            myBarChart_v[i].update();

            myBarChart_i[i].data.labels.splice(0, 1);
            myBarChart_i[i].data.datasets[0].data.splice(0, 1);

            myBarChart_i[i].data.labels.push(chart_data.xvalue[i]);
            myBarChart_i[i].data.datasets[0].data.push(parseFloat(chart_data.yvalues_I[i]));
            $('#last_val_i'+i).html(parseFloat(chart_data.yvalues_I[i])).fadeIn(100000);
            myBarChart_i[i].update();
          }
        }
      },
  });
}

function pushData(lv,ll) {
  myBarChart.data.labels.push(ll);
  myBarChart.data.datasets[0].data.push(lv);
  myBarChart.update();
}

function removeData() {
  myBarChart.data.labels.splice(0, 1);
  myBarChart.data.datasets[0].data.splice(0, 1);
  myBarChart.update();


  $('#last_val_'+<?php echo $sensor->name?>i).html(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i])).fadeIn(100000);

}
  </script>
</body>
</html>
