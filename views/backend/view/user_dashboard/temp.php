<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>

<style>
  .chartWrapper {/* w w w. j a v  a2 s.co  m*/
   position: relative;
}
.chartWrapper > canvas {
   position: absolute;
   left: 0;
   top: 0;
   pointer-events:none;
}
.chartAreaWrapper {
   width: 600px;
   overflow-x: scroll;
}
</style>

<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
      <header class="card-header"> User Dash Board </header>

      <?php
        use Kreait\Firebase\Factory;
        use Kreait\Firebase\ServiceAccount;

      foreach($device as $d){
        $serviceAccount = ServiceAccount::fromJsonFile($main_root_path."/firebase_json/".$d->json_file);
        $firebase = (new Factory)
           ->withServiceAccount($serviceAccount)
           ->withDatabaseUri($d->url)
           ->create();
        $database = $firebase->getDatabase();
        $data=$database->getReference('data')->getSnapshot();
        $data=$data->getValue();
      }

      $d=[];
      $value=[];
      foreach($data as  $key => $year){
        $yer=$key;
        foreach($year as  $key => $month){
          $mon=$key;
          foreach($month as $key=>$day){
            $dy=$key;
            foreach($day as $key=>$hour){
              $h=$key;
              foreach($hour as $key=>$minute){
                $m=$key;
                foreach($minute as $key=>$second){
                  $s=$key;
                  $d[]=$yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s;
                  //$d[] = strtotime($yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s);
                  $value[]=$second;
                }
              }
            }
          }
        }
      }
?>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>
  <style id="compiled-css" type="text/css">

.chartWrapper > canvas {
  position: absolute;
  left: 0;
  top: 0;
  pointer-events: none;
}
.chartAreaWrapper {
  width: 600px;
  overflow-x: scroll;
}
    
  </style>
</head>
<body>

    <div class="chartWrapper">
  <div class="chartAreaWrapper">
  <div class="chartAreaWrapper2">
      <canvas id="chart-FuelSpend" height="300" width="1200"></canvas>
  </div>
  </div>
  <canvas id="axis-FuelSpend" height="300" width="0"></canvas>
</div>


  <script type='text/javascript'>
// function generateLabels() {
//   var chartLabels = [];
//   for (x = 0; x < 100; x++) {
//     chartLabels.push("Label" + x);
//   }
//   return chartLabels;
// }
// function generateData() {
//   var chartData = [];
//   for (x = 0; x < 100; x++) {
//     chartData.push(Math.floor((Math.random() * 100) + 1));
//   }
//   return chartData;
// }
function addData(numData, chart){
      for (var i = 0; i < numData; i++){
          //chart.data.datasets[0].data.push(Math.random() * 100);
          //chart.data.labels.push("Label" + i);
          var newwidth = $('.chartAreaWrapper2').width() +100;
          $('.chartAreaWrapper2').width(newwidth);
    }
}
// var chartData = {
//   labels: generateLabels(),
//   datasets: [{
//     label: "Test Data Set",
//     data: generateData()
//   }]
// };
var xValues = [<?php echo '"'.implode('","', $d ).'"' ?>];
var yValues = [<?php echo '"'.implode('","',  $value ).'"' ?>];
$(function() {
  var canvasFuelSpend = $('#chart-FuelSpend');
  var chartFuelSpend = new Chart(canvasFuelSpend, {
    type: 'line',
    data: {
    labels: xValues,
    datasets: [{
      fill: false,
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor:'rgba(54, 162, 235, 0.2)',
      data: yValues,
      borderWidth: 1,
    }]
  },
    maintainAspectRatio: false,
    responsive: true,
    options: {
      tooltips: {
        titleFontSize: 0,
        titleMarginBottom: 0,
        bodyFontSize: 12
      },
      legend: {
        display: false
      },
      scales: {
       
        xAxes: [{
          ticks: {
            fontSize: 12,
            display: true,
          },
        }],
        yAxes: [{
          ticks: {
            fontSize: 12,
            display: true,

          },
        }]
      },
      // animation: {
      //   onComplete: function() {
      //     var sourceCanvas = chartFuelSpend.chart.canvas;
      //     var copyWidth = chartFuelSpend.scales['y-axis-0'].width - 10;
      //     var copyHeight = chartFuelSpend.scales['y-axis-0'].height + chartFuelSpend.scales['y-axis-0'].top + 10;
      //     var targetCtx = document.getElementById("axis-FuelSpend").getContext("2d");
      //     targetCtx.canvas.width = copyWidth;
      //     targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
      //   }
      // }
    }
  });
  addData(10, chartFuelSpend);
});

function get_graph(){

  var user_id = $('#password_reset_user_id').val();
  var formData = {action:'get_graph',user_id:user_id}; //Array 
  $.ajax({
      url : "<?php echo $config['ajax_url'] ?>",
      type: "POST",
      data : formData,
      success: function(data, textStatus, jqXHR)
      {
          data = JSON.parse(data);
          if(data.type == 'error'){
              toastr.error(data.message);
          }
          if(data.type == 'success'){
              toastr.success(data.message);
              $('#password').val('');
              $('#password_reset_user_id').val('');
          }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          
      }
  });
}
  </script>
</body>
</html>
