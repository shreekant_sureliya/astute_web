<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>
  <style id="compiled-css" type="text/css">

.chartWrapper > canvas {
  position: absolute;
  left: 0;
  top: 0;
  pointer-events: none;
  
}
.chartAreaWrapper {
  margin : 0px;
  /* width: 600px; */
  /* overflow-x: scroll; */
}

tbody.scrollContent 
{
    overflow-x: hidden;
    overflow-y: auto;
    height: 100px;
}

.scrollContent tr td 
{
    padding-right: 22px;
    vertical-align: top;
}
<?php

$user_id = $_SESSION['user_data']['user_id'];
$device = Device::where('user_id',$user_id)->get();
$device_count = $device->count();
?>

</style>
</head>
<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card p-2">
      <header class="card-header"> User Dash Board </header>
  <?php 
  for($i=1;$i<=$device_count;$i++){
    foreach($device as $d){?>
      <div class="row">
      <?php $sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
      foreach($sensors as $sensor){ ?>
          <div class="col-10">
          <div class="card shadow-lg mb-2 p-2 bg-white rounded">       
            <div class="card-body p-0">
            <div class="chartWrapper">
              <div class="chartAreaWrapper">
                  <canvas id="barChart_<?php echo $sensor->name.''.$i;?>" ></canvas>
              </div>
            </div>
            </div>
          </div>
        </div>
      

      <div class="col-2 text-center"> 
        <div class="card shadow-lg p-3 mb-5 bg-white rounded w-100">
          <div class="card-header p-0"><?php echo $sensor->name ?></div>
        <div class="card-body">
          <h3 id="last_val_<?php echo $sensor->name.''.$i?>">0</h3>
        </div>
        </div>
      </div>
      <?php } ?>
      </div>
      <hr>
  <?php  break;  } ?>
<?php } ?>
</div>
</body>
</html>



<script type='text/javascript'>
// var canvas = document.getElementById("barChart");
// var canvas1 = document.getElementById("barChart1");
// var ctx = canvas.getContext('2d');
// var ctx1 = canvas1.getContext('2d');
// var chartType = 'line';
  <?php
  for($i=1;$i<=$device_count;$i++){
  foreach($device as $d){
    $sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
    foreach($sensors as $sensor){ ?>
      var myBarChart_<?php echo $sensor->name?> = [];    
    <?php } ?>
  <?php } ?>
  <?php } ?>
  
  // var myBarChart1;
  //var myBarChart=[];
  // var ctx=[];

  var formData = {action:'get_graph',no:9}; //Array 
    $.ajax({
      url : "<?php echo $config['ajax_url'] ?>",
      type: "POST",
      data : formData,
      success: function(data, textStatus, jqXHR)
      {
        data = JSON.parse(data);
        if(data.type == 'success'){
          //$('#last_val').html(data.last_val);
          //generate_graph(data.xvalue,data.yvalue,data.max,data.min);
          //console.log(data.xvalue);
          //console.log(data.xvalue_1);

          for(var i=1;i<= <?php echo $device_count; ?>;i++){
            //<?php 
              //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
              foreach($sensors as $sensor){ ?>

                  var canvas<?php echo $sensor->name?> = document.getElementById("barChart_<?php echo $sensor->name?>"+i);
                  var ctx<?php echo $sensor->name?> = canvas<?php echo $sensor->name?>.getContext('2d');
                  var chartType = 'line';
                  //console.log(data.xvalue[i]);
                  xv= data.xvalue[i];
                  yv<?php echo $sensor->name?>=data.yvalues_<?php echo $sensor->name?>[i];


                  var d<?php echo $sensor->name?> = {
                    labels:xv,
                    datasets: [{
                      label: "Device "+i,
                      fill: true,
                      lineTension: 0.5,
                      backgroundColor: "#e0ecfc",
                      borderColor: "#1f3bb3", // The main line color
                      borderCapStyle: 'square',
                      pointBorderColor: "white",
                      pointBackgroundColor: "#1f3bb3",
                      pointBorderWidth: 1,
                      pointHoverRadius: 8,
                      pointHoverBackgroundColor: "#1f3bb3",
                      pointHoverBorderColor: "#e0ecfc",
                      pointHoverBorderWidth: 2,
                      pointRadius: 4,
                      pointHitRadius: 10,
                      data: yv<?php echo $sensor->name?>,
                      spanGaps: true,
                    }],
                  }

                  //myBarChart = myBarChart[i];
                  //ctx = ctx[i];
                  myBarChart_<?php echo $sensor->name?>[i] = new Chart(ctx<?php echo $sensor->name?>, {
                    type: chartType,
                    data: d<?php echo $sensor->name?>,
                    options: options
                  });
            <?php } ?>
          }
        }
      },
    });

  var options = {
    scales: {
        xAxes: [{
          ticks: {
            fontSize: 12,
            display: true,
            max:1000,
            min:10,
          },
          gridLines: {
                  display:false
            }     
        }],
        yAxes: [{
          ticks: {
            fontSize: 12,
            display: true,
          }, 
        }]
      },
    title: {
      fontSize: 18,
      display: true,
      position: 'bottom'
    }
  };


  // function init(d) {
  //   // Chart declaration:
  //   myBarChart = new Chart(ctx, {
  //     type: chartType,
  //     data: d,
  //     options: options
  //   });

  //   myBarChart1 = new Chart(ctx1, {
  //     type: chartType,
  //     data: d,
  //     options: options
  //   });
  // }

  $(document).ajaxComplete(function () {
    get_data();
    setInterval(function() {
      get_data();
    }, 5000);
  });

  function get_data(){
    var formData = {action:'get_graph',no:1}; //Array 
    $.ajax({
        url : "<?php echo $config['ajax_url'] ?>",
        type: "POST",
        data : formData,
        success: function(d, textStatus, jqXHR)
        {
          chart_data = JSON.parse(d);
          var last = get_last_values();
        
        //console.log(last);
        //console.log(typeof last[1]);
          if(chart_data.type == 'success'){
            for(var i=1;i<= <?php echo $device_count; ?>;i++){
              //console.log(chart_data.yvalue[i]);
              //myBarChart = myBarChart[i];
              //console.log(myBarChart.data.labels);
              // removeData();
              // pushData(chart_data.xvalue[i],chart_data.yvalue[i]);
              //console.log(chart_data.xvalue[i]);
              //myBarChart = myBarChart+i
              // console.log(myBarChart.data.labels);
              //myBarChart = myBarChart+i;
              //var yVal = 15
              //yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
              
              <?php 
                //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
                foreach($sensors as $sensor){ ?>
                  var a = parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i][0]);
                          
                  if(last.includes(a)){
                    break;
                  }
                  
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.push(chart_data.xvalue[i]);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.push(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  $('#last_val_<?php echo $sensor->name?>'+i).html(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  myBarChart_<?php echo $sensor->name?>[i].update();

                <?php } ?>
            }
          }
        },
    });
  }

  function get_last_values(){
    var last_data = [];
    last_data.push(null);
    for(var i=1;i<= <?php echo $device_count; ?>;i++){           
    <?php 
      //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
      foreach($sensors as $sensor){ ?>
        last_data.push(parseFloat($('#last_val_<?php echo $sensor->name?>'+i).html()));
      <?php } ?>
      }
      return last_data;
    }



  // function pushData(lv,ll) {
  //   myBarChart.data.labels.push(ll);
  //   myBarChart.data.datasets[0].data.push(lv);
  //   myBarChart.update();
  // }

  // function removeData() {
  //   myBarChart.data.labels.splice(0, 1);
  //   myBarChart.data.datasets[0].data.splice(0, 1);
  //   myBarChart.update();
  // }


    </script>
</body>
</html>