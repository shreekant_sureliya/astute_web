<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>
  <style id="compiled-css" type="text/css">

.chartWrapper > canvas {
  position: absolute;
  left: 0;
  top: 0;
  pointer-events: none;
}
.chartAreaWrapper {
  width: 600px;
  /* overflow-x: scroll; */
}
    
  </style>
</head>
<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
      <header class="card-header"> User Dash Board </header>

<html>
<body>
<div class="row p-5">
  <div class="col">
    <div class="chartWrapper">
    <div class="chartAreaWrapper">
    <div class="chartAreaWrapper2">
        <canvas id="chart-FuelSpend" height="600" width="1200"></canvas>
    </div>
    </div>
    <canvas id="axis-FuelSpend" height="300" width="0"></canvas>
  </div>
  </div>
  <div class="col text-center">
          <h3 id="last_val">0</h3>
    </div>
</div>
</body>
</html>

  <script type='text/javascript'>
// function generateLabels() {
//   var chartLabels = [];
//   for (x = 0; x < 100; x++) {
//     chartLabels.push("Label" + x);
//   }
//   return chartLabels;
// }
// function generateData() {
//   var chartData = [];
//   for (x = 0; x < 100; x++) {
//     chartData.push(Math.floor((Math.random() * 100) + 1));
//   }
//   return chartData;
// }
function addData(numData, chart){
    //   for (var i = 0; i < numData; i++){
    //       //chart.data.datasets[0].data.push(Math.random() * 100);
    //       //chart.data.labels.push("Label" + i);
    //       var newwidth = $('.chartAreaWrapper2').width() +20;
    //       $('.chartAreaWrapper2').width(newwidth);
    // }
}

function generate_graph(xValues,yValues,max_val,min_val) {
  var canvasFuelSpend = $('#chart-FuelSpend');
  var chartFuelSpend = new Chart(canvasFuelSpend, {
    type: 'line',
    data: {
    labels: xValues,
    datasets: [{
      fill: false,
      borderColor: 'rgb(54, 162, 235)',
      backgroundColor:'rgba(54, 162, 235, 0.2)',
      data: yValues,
      borderWidth: 1,
    }]
  },
    maintainAspectRatio: false,
    responsive: true,
    options: {
      tooltips: {
        titleFontSize: 0,
        titleMarginBottom: 0,
        bodyFontSize: 12
      },
      legend: {
        display: false
      },
      scales: {
       
        xAxes: [{
          ticks: {
            fontSize: 12,
            display: true,
            max:max_val,
            min:min_val,
          },
        }],
        yAxes: [{
          ticks: {
            fontSize: 12,
            display: true,
          },
        }]
      },
      // animation: {
      //   onComplete: function() {
      //     var sourceCanvas = chartFuelSpend.chart.canvas;
      //     var copyWidth = chartFuelSpend.scales['y-axis-0'].width - 10;
      //     var copyHeight = chartFuelSpend.scales['y-axis-0'].height + chartFuelSpend.scales['y-axis-0'].top + 10;
      //     var targetCtx = document.getElementById("axis-FuelSpend").getContext("2d");
      //     targetCtx.canvas.width = copyWidth;
      //     targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
      //   }
      // }
    }
  });
  addData(10, chartFuelSpend);
}

$(document).ready(function(){
  graph();
  // setInterval(function() {
  //   graph();
  // }, 5000);
  
});

function graph(){
  var formData = {action:'get_graph'}; //Array 
  $.ajax({
      url : "<?php echo $config['ajax_url'] ?>",
      type: "POST",
      data : formData,
      success: function(data, textStatus, jqXHR)
      {
        data = JSON.parse(data);
        if(data.type == 'success'){
          $('#last_val').html(data.last_val);
          generate_graph(data.xvalue,data.yvalue,data.max,data.min);
        }
      },
  });
}
  </script>
</body>
</html>
