<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
    <header class="card-header">
	<div class="form-row">
		<div class="form-group col col-md-5">
		<label for="inputEmail4">Select Device</label>
		<select class="form-control form-control-lg" id="device">
			<?php foreach($devices as $device){ ?>
					<option value="<?php echo $device->id ?>"><?php echo $device->device_id ?></option>
			<?php } ?>
		</select>
		</div>
		<div class="form-group col col-md-4">
		<label for="inputEmail4">Date  : </label>
			<input id="date" type="text" name="date" class="form-control datepicker_range">
		</div>
		<div class="form-group col col-md-2" style="padding-top: 33px;">
			<button class="btn-primary btn-sm" id="btn_go">Go</button>
			<button class="btn-info btn-sm" id="export">export</button>
		</div>
  </div>
	</header>
      <div class="card-body"> 
				<section id="flip-scroll">
					<table class="table" id="datatable">
						
					</table>
				</section>
			</div>
    </section>
  </div>
</div>