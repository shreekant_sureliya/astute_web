<script>
    $(document).on('click','#btn_go',function(){
        var id = $('#device').val();
        var date = $('#date').val();
        var formData = {action:'get_tabledata',id:id,date:date}; //Array 
        $.ajax({
            url : "<?php echo $config['ajax_url'] ?>",
            type: "POST",
            data : formData,
            success: function(data, textStatus, jqXHR)
            {
                data = JSON.parse(data);
                if(data.type == 'error'){
                        toastr.error(data.message);
                }
                if(data.type == 'success'){
                    $('#datatable').html(data.table_row);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                        
            }
        });
    });

    let options = {
	"separator": ",",
	"newline": "\n",
	"quoteFields": true,
	"excludeColumns": "",
	"excludeRows": "",
	"trimContent": true,
	"filename": "table.csv",
	"appendTo": "#output"
}

$(document).on('click','#export',function(){
    // $('#datatable').csvExport({
    //     title:'tbl_1'
    // });
    $('#datatable').table2csv({
        file_name: 'data.csv'
    });

});
</script>