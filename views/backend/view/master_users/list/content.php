<header class="card-header">
	<h5>
		User List
		<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users&action=add" class="btn btn-sm btn-primary pull-right">Add New User</a>
	</h5>
</header>
<div class="card-body"> 
	<section id="flip-scroll">
		<table class="table table-bordered table-striped table-condensed cf">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>UserName</th>
					<th>Contact No</th>
					<th>Active Module</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach($users as $user){ ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $user->first_name ?> <?php echo $user->last_name ?></td>
						<td><?php echo $user->email ?></td>
						<td><?php echo $user->contact_no ?></td>
						<td>-</td>
						<td>
							<?php
							if($user->is_active == 1){
								echo '<span class="badge badge-success">Active</span>';
							}else{
								echo '<span class="badge badge-danger">Deactivate</span>';
							} ?>
						</td>
						<td>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users&action=edit&user_id=<?php echo $user->user_id; ?>" class="btn btn-primary btn-xs">Edit</a>
	
							<?php
							if($user->is_active == 1){ ?>
								<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users&action=disable_user&user_id=<?php echo $user->user_id; ?>" class="btn btn-danger btn-xs">Disable</a>
							<?php }else{ ?>
								<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users&action=enable_user&user_id=<?php echo $user->user_id; ?>" class="btn btn-success btn-xs">Enable</a>
							<?php } ?>
							
							<button class="btn btn-warning btn-xs" onclick="open_model('<?php echo $user->user_id; ?>','<?php echo $user->first_name ?> <?php echo $user->last_name ?>')">Reset Password</button>
						</td>
					</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</section>
</div>
<!-- password reset model start here -->
<div id="password_reset" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
				<h5 class="modal-title">Change Password For '<span id="full_name"></span>'</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">x</span>
				</button>
			</div>
      <div class="modal-body">
        <input type="hidden" id="password_reset_user_id" value="">
        <form class="form-horizontal">
          <div class="form-group">
            <label for="password" class="col-sm-12 control-label">Password</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="password" placeholder="Enter Password">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" onclick="reset_password()">Reset Password</button>
      </div>
    </div>
  </div>
</div>
<!-- password reset model end here -->
