<header class="card-header"><h5>Edit User</h5></header>
<div class="card-body">
	<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/update_master_user.php" method="post">
		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
		<div class="form-group row">
			<label for="first_name" class="col-sm-2 control-label">First Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" value="<?php echo $user->first_name; ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="last_name" class="col-sm-2 control-label">Last Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="<?php echo $user->last_name; ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="email" class="col-sm-2 control-label">UserName</label>
			<div class="col-sm-10">
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Username as Email-Id" value="<?php echo $user->email; ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="contact_no" class="col-sm-2 control-label">Contact No</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" value="<?php echo $user->contact_no; ?>">
			</div>
		</div>
		<div class="form-group row">
			<label for="expiry_date" class="col-sm-2 control-label">Expirty Date</label>
			<div class="col-sm-10">
			
				<input type="text" class="form-control date-picker-input" name="expiry_date" id="expiry_date" placeholder="Enter Expiry Date">
			</div>
		</div>
		<div class="box-footer">
			<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users" class="btn btn-default">Cancel</a>
			<button type="submit" class="btn btn-info pull-right">Update User</button>
		</div>
	</form>
</div>