<header class="card-header"><h5>Edit User</h5></header>
<div class="card-body">
	<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/add_master_user.php" method="post">
		<div class="form-group row">
			<label for="first_name" class="col-sm-2 control-label">First Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name">
			</div>
		</div>
		<div class="form-group row">
			<label for="last_name" class="col-sm-2 control-label">Last Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
			</div>
		</div>
		<div class="form-group row">
			<label for="email" class="col-sm-2 control-label">UserName</label>
			<div class="col-sm-10">
				<input type="email" class="form-control" name="email" id="email" placeholder="Enter Username as Email-Id">
			</div>
		</div>
		<div class="form-group row">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
			</div>
		</div>
		<div class="form-group row">
			<label for="contact_no" class="col-sm-2 control-label">Contact No</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No">
			</div>
		</div>
		<div class="form-group row">
			<label for="expiry_date" class="col-sm-2 control-label">Expirty Date</label>
			<div class="col-sm-10">
				<input type="text" class="form-control date-picker-input" name="expiry_date" id="expiry_date" placeholder="Enter Expiry Date">
			</div>
		</div>
		<?php /*?><div class="form-group">
			<label for="modules" class="col-sm-2 control-label">Modules</label>
			<div class="col-sm-10">
	
				<?php foreach($Modules as $m){ ?>
					<div class="checkbox">
							<label>
								<input type="checkbox" name="module[]" value="<?php echo $m->module_id ?>">
								<?php echo $m->module_display_name ?>
							</label>
					</div>
				<?php } ?>
	
			</div>
		</div><?php */?>
		<div class="box-footer">
			<a href="<?php echo $config['site_url'] ?>/index.php?view=master_users" class="btn btn-default">Cancel</a>
			<button type="submit" class="btn btn-info pull-right">Add User</button>
		</div>
	</form>
</div>