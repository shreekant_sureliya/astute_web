<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
		<?php
			if($view_mode == 'list'){
				include('list/content.php');
			}
			if($view_mode == 'add'){
				include('add/content.php');
			}
			if($view_mode == 'edit'){
				include('edit/content.php');
			}
		?>
    </section>
  </div>
</div>