<?php

$view_mode     = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : 'list';
$item_per_page = 10;

if ($view_mode == 'disable_user') {

    $user_id         = get_form_value('user_id');
    $user            = User::find($user_id);
    $user->is_active = 0;
    $user->save();
    FlashMessage::set('User Has Been Disabled', 'success');
    redirect($_SERVER['HTTP_REFERER'], 'full');

}

if ($view_mode == 'enable_user') {

    $user_id         = get_form_value('user_id');
    $user            = User::find($user_id);
    $user->is_active = 1;
    $user->save();
    FlashMessage::set('User Has Been Enabled', 'success');
    redirect($_SERVER['HTTP_REFERER'], 'full');

}

if ($view_mode == 'list') {
    $users = User::where(array('user_type' => 'admin'))->get();
}
if ($view_mode == 'add') {
    }

if ($view_mode == 'edit') {

    $user_id      = get_form_value('user_id');
    $user         = User::find($user_id);

}
