<script type="text/javascript">
	
	function open_model(user_id,fullname){

			$('#full_name').html(fullname);
			$('#password_reset_user_id').val(user_id);
			$('#password_reset').modal('show');

	}

	function reset_password(){

			var user_id = $('#password_reset_user_id').val();
			var password = $('#password').val();

			if(password == ''){
				toastr.error("Please Enter Password..");
				$('#password').focus();
			}else{

				$('#password_reset').modal('hide');

			var formData = {action:'reset_password',user_id:user_id,password:password}; //Array 
 
			$.ajax({
			    url : "<?php echo $config['ajax_url'] ?>",
			    type: "POST",
			    data : formData,
			    success: function(data, textStatus, jqXHR)
			    {
			        data = JSON.parse(data);
			        if(data.type == 'error'){
			        		toastr.error(data.message);
			        }
			        if(data.type == 'success'){
			        		toastr.success(data.message);
			        		$('#password').val('');
			        		$('#password_reset_user_id').val('');
			        }

			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 				
			    }
			});

			}
	}

</script>