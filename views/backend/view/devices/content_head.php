<?php
		
	$view_mode = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : 'list';
	$item_per_page = 10;

	if($view_mode == 'list'){
		$devices = Device::with('user')->get();
	}

	if($view_mode == 'add'){
		$users = User::where('is_active',1)->where('user_type','user')->get();
		$sensor = Sensor::where('is_active',1)->get();
	}

	if($view_mode == 'edit'){
		$id = get_form_value('id');
		$device = Device::find($id);
		$sensor = Sensor::where('is_active',1)->get();
		$users = User::where('is_active',1)->where('user_type','user')->get();
	}

	if($view_mode == 'delete'){
		$id = get_form_value('id');
		$device = Device::find($id);
		$device->delete();
		FlashMessage::set('Device Has Been Deleted','success');
		redirect($_SERVER['HTTP_REFERER'],'full');
	}
	
?>