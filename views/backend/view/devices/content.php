<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
			<?php if($view_mode == 'list'){?>
      <header class="card-header">
				<h5>
					Device List
					<a href="<?php echo $config['site_url'] ?>/index.php?view=devices&action=add" class="btn btn-sm btn-primary pull-right">New Device</a>
				
				</h5>
			</header>
      <div class="card-body"> 
				<section id="flip-scroll">
					<table class="table">
						<thead class="cf">
							<tr>
								<th>No</th>
								<th>Device Id</th>
								<th>User Name</th>
								<th>Url</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($devices as $device){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $device->device_id ?></td>
							<td><?php echo $device->user->user_name ?></td>
							<td><?php echo $device->url ?></td>
							<td>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=devices&action=edit&id=<?php echo $device->id; ?>" class="btn btn-primary btn-xs">Edit</a>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=devices&action=delete&id=<?php echo $device->id; ?>" class="btn btn-danger btn-xs">Delete</a>
							</td>
						</tr>
						<?php $i++; } ?>
						</tbody>
					</table>
				</section>
			</div>
			<?php }?>

			<?php if($view_mode == 'add'){?>
			<header class="card-header"><h5>New Device</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/devices/add_device.php" method="post" enctype="multipart/form-data">
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Device Id</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="device_id" id="device_id" placeholder="Enter Device Id" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="url" id="url" placeholder="Enter URL" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Select User</label>
						<div class="col-sm-10">
							<select name="user_id" id="user_id" class="form-control"  required>
								<option value="">Please Select User</option>
								<?php foreach($users as $user) {?>
								<option value="<?php echo $user->user_id?>"><?php echo $user->user_name?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Select Sensor</label>
						<div class="col-sm-10">
							<select name="sensor_id[]" id="sensor_id" class="form-control"  required multiple>
								<option value="">Please Select Sensor</option>
								<?php foreach($sensor as $sensor) {?>
								<option value="<?php echo $sensor->id?>"><?php echo $sensor->name?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="gmail" class="col-sm-2 control-label">Gmail</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="gmail" id="gmail" placeholder="Enter Gmail" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">File Upload</label>
						<div class="col-sm-10">
						  <input type="file" name="file" id="file">
						</div>
					</div>
					<div class="box-footer">
					<button type="submit" class="btn btn-info">Add Device</button>
					<a href="<?php echo $config['site_url'] ?>/index.php?view=devices" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php }?>

			<?php if($view_mode == 'edit'){?>
			<header class="card-header"><h5>Edit Device</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/devices/update_device.php" method="post" enctype="multipart/form-data">

					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Device Id</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="device_id" id="device_id" placeholder="Enter Device Id" value="<?php echo $device->device_id?>" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">URL</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="url" id="url" placeholder="Enter URL" value="<?php echo $device->url?>"required>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Select User</label>
						<div class="col-sm-10">
							<select name="user_id" id="user_id" class="form-control"  required>
								<option value="">Please Select User</option>
								<?php foreach($users as $user) {
									if($user->user_id == $device->user_id){ ?>
										<option value="<?php echo $user->user_id?>" selected><?php echo $user->user_name?></option>
									<?php } else { ?>
										<option value="<?php echo $user->user_id?>"><?php echo $user->user_name?></option>
								<?php } } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="device_id" class="col-sm-2 control-label">Select Sensor</label>
						<div class="col-sm-10">
							<select name="sensor_id[]" id="sensor_id" class="form-control"  required multiple>
								<option value="">Please Select Sensor</option>
									<?php foreach($sensor as $sensor) {
										if(in_array($sensor->id,json_decode($device->sensor))){ ?>

											<option value="<?php echo $sensor->id?>" selected><?php echo $sensor->name?></option>

										<?php }else{ ?>

											<option value="<?php echo $sensor->id?>" ><?php echo $sensor->name?></option>

									<?php } } ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="gmail" class="col-sm-2 control-label">Gmail</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="gmail" id="gmail" placeholder="Enter Gmail" value="<?php echo $device->gmail?>" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="password" id="password" placeholder="Enter Password" value="<?php echo $device->password?>" required>
						</div>
					</div>
					<?php if($device->json_file){
						echo 'file already uploaded';
					}
					?>
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">File Upload</label>
						<div class="col-sm-10">
						  <input type="file" name="file" id="file">
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Update Device</button>
						<a href="<?php echo $config['site_url'] ?>/index.php?view=devices" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php } ?>
    </section>
  </div>
</div>