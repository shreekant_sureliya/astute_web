<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
			<?php if($view_mode == 'list'){?>
      <header class="card-header">
				<h5>
					User List
					<a href="<?php echo $config['site_url'] ?>/index.php?view=users&action=add" class="btn btn-sm btn-primary pull-right">New User</a>
				
				</h5>
			</header>
      <div class="card-body"> 
				<section id="flip-scroll">
					<table class="table">
						<thead class="cf">
							<tr>
								<th>No</th>
								<th>Name</th>
								<th>Email</th>
								<th>Contact No</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($users as $user){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo ucwords($user->user_name); ?></td>
							<td><?php echo $user->email ?></td>
							<td><?php echo $user->contact_no ?></td>
							<td>
							<?php
							if($user->is_active == 1){
								echo '<span class="badge badge-success">Active</span>';
							}else{
								echo '<span class="badge badge-danger">Deactivate</span>';
							} ?>
							</td>
							<td>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=users&action=edit&user_id=<?php echo $user->user_id; ?>" class="btn btn-primary btn-xs">Edit</a>
		
							<?php
							if($user->is_active == 1){ ?>
								<a href="<?php echo $config['site_url'] ?>/index.php?view=users&action=disable_user&user_id=<?php echo $user->user_id; ?>" class="btn btn-danger btn-xs">Disable</a>
							<?php }else{ ?>
								<a href="<?php echo $config['site_url'] ?>/index.php?view=users&action=enable_user&user_id=<?php echo $user->user_id; ?>" class="btn btn-success btn-xs">Enable</a>
							<?php } ?>
		
							<button class="btn btn-warning btn-xs" onclick="open_model('<?php echo $user->user_id; ?>','<?php echo $user->user_name ?> <?php echo $user->last_name ?>')">Reset Password</button>
							</td>
						</tr>
						<?php $i++; } ?>
						</tbody>
					</table>
				</section>
			</div>
			<div id="password_reset" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Change Password For '<span id="full_name"></span>'</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">x</span>
							</button>
						</div>
						<div class="modal-body">
							<input type="hidden" id="password_reset_user_id" value="">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="password" class="col-sm-12 control-label">Password</label>
									<div class="col-sm-12">
										<input type="text" class="form-control" id="password" placeholder="Enter Password" />
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" onclick="reset_password()">Reset Password</button>
						</div>
					</div>
				</div>
			</div>
			<?php }?>

			<?php if($view_mode == 'add'){?>
			<header class="card-header"><h5>New User</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/add_user.php" method="post" onsubmit="return user_validate();">
					<div class="form-group row">
						<label for="user_name" class="col-sm-2 control-label">User Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="user_name" id="user_name" placeholder="Enter User Name" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
						<input type="password" class="form-control" name="password" id="password" placeholder="Enter password" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="contact_no" class="col-sm-2 control-label">Contact No</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="contact_no" class="col-sm-2 control-label">Permission</label>
						<div class="col-sm-10">
							<div class="checkbox">
							<?php foreach($all_permission_list as $p){?>
								<label><input type="checkbox" name="permission[]" id="permission" value="<?php echo $p;?>" /> <?php echo ucwords(str_replace('_', ' ', $p));?></label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php }?>
							</div>
						</div>
					</div>
					<div class="box-footer">
					<button type="submit" class="btn btn-info">Add User</button>
					<a href="<?php echo $config['site_url'] ?>/index.php?view=users" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php }?>

			<?php if($view_mode == 'edit'){?>
			<header class="card-header"><h5>Edit User</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/update_user.php" method="post" onsubmit="return update_user_validate();">
					<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
					<div class="form-group row">
						<label for="user_name" class="col-sm-2 control-label">User Name</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="user_name" id="user_name" placeholder="Enter First Name" value="<?php echo $user->user_name; ?>">
						</div>
					</div>
					
					<div class="form-group row">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="email" id="email" placeholder="Enter Username as Email-Id" value="<?php echo $user->email; ?>">
						</div>
					</div>
	
					<div class="form-group row">
						<label for="contact_no" class="col-sm-2 control-label">Contact No</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" value="<?php echo $user->contact_no; ?>">
						</div>
					</div>

					<div class="form-group row">
						<label for="contact_no" class="col-sm-2 control-label">Permission</label>
						<div class="col-sm-10">
							<div class="checkbox">
							<?php 
							foreach($all_permission_list as $p){
							if(in_array($p, $user_allowed_permission)){
								$cls = 'checked="checked"';
							}else{
								$cls = '';
							}
							
							?>
								<label><input type="checkbox" name="permission[]" id="permission" <?php echo $cls;?> value="<?php echo $p;?>" /> <?php echo ucwords(str_replace('_', ' ', $p));?></label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php }?>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Update User</button>
						<a href="<?php echo $config['site_url'] ?>/index.php?view=users" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php } ?>
    </section>
  </div>
</div>