<div class="row state-overview">
  <div class="col-lg-12">
    <section class="card">
			<?php if($view_mode == 'list'){?>
      <header class="card-header">
				<h5>
					Sensor List
					<a href="<?php echo $config['site_url'] ?>/index.php?view=sensors&action=add" class="btn btn-sm btn-primary pull-right">New Sensor</a>
				
				</h5>
			</header>
      <div class="card-body"> 
				<section id="flip-scroll">
					<table class="table">
						<thead class="cf">
							<tr>
								<th>No</th>
								<th>Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($sensors as $sensor){ ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $sensor->name ?></td>
							<td>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=sensors&action=edit&id=<?php echo $sensor->id; ?>" class="btn btn-primary btn-xs">Edit</a>
							<a href="<?php echo $config['site_url'] ?>/index.php?view=sensors&action=delete&id=<?php echo $sensor->id; ?>" class="btn btn-danger btn-xs">Delete</a>
							</td>
						</tr>
						<?php $i++; } ?>
						</tbody>
					</table>
				</section>
			</div>
			<?php }?>

			<?php if($view_mode == 'add'){?>
			<header class="card-header"><h5>New Sensor</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/sensors/add_sensor.php" method="post">
					<div class="form-group row">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>
						</div>
					</div>
					<div class="box-footer">
					<button type="submit" class="btn btn-info">Add Sensor</button>
					<a href="<?php echo $config['site_url'] ?>/index.php?view=sensors" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php }?>

			<?php if($view_mode == 'edit'){?>
			<header class="card-header"><h5>Edit Sensor</h5></header>
			<div class="card-body">
				<form class="form-horizontal" action="<?php echo $config['form_action_url'] ?>/sensors/update_sensor.php" method="post" enctype="multipart/form-data">

					<input type="hidden" name="id" value="<?php echo $id; ?>">

					<div class="form-group row">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $sensor->name?>" required>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-info">Update Sensor</button>
						<a href="<?php echo $config['site_url'] ?>/index.php?view=sensors" class="btn btn-default">Cancel</a>
					</div>
				</form>
			</div>
			<?php } ?>
    </section>
  </div>
</div>