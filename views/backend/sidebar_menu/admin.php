<?php

$side_ui_menu = new SidebarMenu;
if(checkPermition('dashboard')){
$side_ui_menu->add(array(
    'type'  => 'menu',
    'icon'  => 'fa fa-dashboard',
    'text'  => 'Dashboard',
    'route' => $config['site_url'] . '/index.php',
    'page'  => 'dashboard',
    'class' => '',
    'id'    => 'dashboard',
    'extra' => '',
));
}

if(checkPermition('users')){
  $side_ui_menu->add(array(
      'type'  => 'menu',
      'icon'  => 'fa fa-users',
      'text'  => 'All Users',
      'route' => $config['site_url'] . '/index.php?view=users',
      'page'  => 'users',
      'class' => '',
      'id'    => 'users',
      'extra' => '',
  ));
}

if(checkPermition('devices')){
  $side_ui_menu->add(array(
      'type'  => 'menu',
      'icon'  => 'fa fa-users',
      'text'  => 'Devices',
      'route' => $config['site_url'] . '/index.php?view=devices',
      'page'  => 'devices',
      'class' => '',
      'id'    => 'devices',
      'extra' => '',
  ));
}

if(checkPermition('sensors')){
    $side_ui_menu->add(array(
        'type'  => 'menu',
        'icon'  => 'fa fa-users',
        'text'  => 'Sensors',
        'route' => $config['site_url'] . '/index.php?view=sensors',
        'page'  => 'sensors',
        'class' => '',
        'id'    => 'sensors',
        'extra' => '',
    ));
  }
/*
$side_ui_menu->add(array(
    'type'  => 'menu',
    'icon'  => 'fa fa-database',
    'text'  => 'Ebay Accounts',
    'route' => $config['site_url'] . '/index.php?view=ebay_users',
    'page'  => 'ebay_users',
    'class' => '',
    'id'    => 'ebay_users',
    'extra' => '',
));


$side_ui_menu->add(array(
    'type'     => 'menu',
    'icon'     => 'fa fa-shopping-cart',
    'text'     => 'Ebay Orders Management',
    'route'    => '#',
    'page'     => 'ebay_order',
    'class'    => '',
    'id'       => 'ebay_order',
    'extra'    => '',
    'sub_menu' => [
        array(
            'type'  => 'menu',
            'icon'  => 'fa fa-database',
            'text'  => 'Ebay Orders',
            'route' => $config['site_url'] . '/index.php?view=ebay_orders',
            'page'  => 'ebay_orders',
            'class' => '',
            'id'    => 'ebay_orders',
            'extra' => '',
        ),
    ],
));*/

$data['side_ui_menu'] = $side_ui_menu->get_menu();
