<?php

$side_ui_menu = new SidebarMenu;
if(checkPermition('user_dashboard')){
$side_ui_menu->add(array(
    'type'  => 'menu',
    'icon'  => 'fa fa-dashboard',
    'text'  => 'Dashboard',
    'route' => $config['site_url'] . '/index.php',
    'page'  => 'user_dashboard',
    'class' => '',
    'id'    => 'user_dashboard',
    'extra' => '',
));
}

/*$side_ui_menu->add(array(
    'type'  => 'menu',
    'icon'  => 'fa fa-sitemap',
    'text'  => 'Devices',
    'route' => $config['site_url'] . '/index.php?view=devices',
    'page'  => 'devices',
    'class' => '',
    'id'    => 'devices',
    'extra' => '',
));*/
if(checkPermition('data')){
$side_ui_menu->add(array(
    'type'  => 'menu',
    'icon'  => 'fa fa-tasks',
    'text'  => 'Data',
    'route' => $config['site_url'] . '/index.php?view=data',
    'page'  => 'data',
    'class' => '',
    'id'    => 'data',
    'extra' => '',
));
}

$data['side_ui_menu'] = $side_ui_menu->get_menu();
