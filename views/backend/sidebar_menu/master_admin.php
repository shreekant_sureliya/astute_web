<?php 
	
			 $side_ui_menu = new SidebarMenu;

       $side_ui_menu->add(array(
                            'type'  => 'menu',
                            'icon'  => 'fa fa-dashboard',
                            'text'  => 'Dashboard',
                            'route' => $config['site_url'].'/index.php',
                            'page' => 'dashboard',
                            'class' => '',
                            'id'    => 'dashboard',
                            'extra' => ''
                        ));

       $side_ui_menu->add(array(
                            'type'  => 'menu',
                            'icon'  => 'fa fa-users',
                            'text'  => 'All Users',
                            'route' => $config['site_url'].'/index.php?view=master_users',
                            'page' => 'master_users',
                            'class' => '',
                            'id'    => 'master_users',
                            'extra' => '',
                        ));

       $side_ui_menu->add(array(
                            'type'  => 'menu',
                            'icon'  => 'fa fa-cogs',
                            'text'  => 'Site Settings',
                            'route' => '#',
                            'page' => 'site_settings',
                            'class' => '',
                            'id'    => 'site_settings',
                            'extra' => '',
                            'sub_menu' => [
                                      array(
                                          'type'  => 'menu',
                                          'icon'  => 'fa fa-database',
                                          'text'  => 'Permission',
                                          'route' => 'index.php?view=permission',
                                          'page' => 'permission',
                                          'class' => '',
                                          'id'    => 'permission',
                                          'extra' => '',
                                      ),
                                  ]
                        ));

       $data['side_ui_menu'] = $side_ui_menu->get_menu();

?>
