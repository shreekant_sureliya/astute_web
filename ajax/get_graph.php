<?php 
$user_id = $_SESSION['user_data']['user_id'];
$device = Device::where('user_id',$user_id)->get();

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$no = get_form_value('no');
$i=1;
$j=1;
foreach($device as $d){
$serviceAccount = ServiceAccount::fromJsonFile($main_root_path."/firebase_json/".$d->json_file);
$firebase = (new Factory)
   ->withServiceAccount($serviceAccount)
   ->withDatabaseUri($d->url)
   ->create();
$database = $firebase->getDatabase();
//$data=$database->getReference('data')->getSnapshot();

$data=$database->getReference('data')
    ->orderByChild('data')
    ->limitToLast($no)
    ->getSnapshot();

// $updates = [
//   'XYZ' => '123',
//   'ABC' => '321',
// ];

// $database->getReference('data') // this is the root reference
//  ->update($updates);

$data=$data->getValue();
$xvalues=[];
$yvalues=[];

// $sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
// foreach($data as $key=>$val){
//   foreach($sensors as $sensor){
//     $yvalues[$sensor->name]=[];
//   }
// }

date_default_timezone_set("Asia/Kolkata");

$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
foreach($data as $key=>$val){
  $xvalues[] = date('h:i:s', $key);
  foreach($sensors as $sensor){
    $yvalues[$sensor->name][] = $val[$sensor->name];
  }
}
$out['type'] = 'success';
$out['xvalue'][$i] = $xvalues;
foreach($sensors as $sensor){
  $out['yvalues_'.$sensor->name][$i] = $yvalues[$sensor->name];
}
$i++;
}



// $d=[];
// $value=[];
// foreach($data as  $key => $year){
// $yer=$key;
// foreach($year as  $key => $month){
//   $mon=$key;
//   foreach($month as $key=>$day){
//     $dy=$key;
//     foreach($day as $key=>$hour){
//       $h=$key;
//       foreach($hour as $key=>$minute){
//         $m=$key;
//         foreach($minute as $key=>$second){
//           $s=$key;
//           $d[]=$yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s;
//           //$d[] = strtotime($yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s);
//           $value[]=$second;
//         }
//       }
//     }
//   }
// }
// }

// $out['type'] = 'success';
// $out['xvalue'] = array_slice($d,-9,9);
// $out['yvalue'] = array_slice($value,-9,9);
// $out['max'] = max($value);
// $out['min'] = min($value);
// $out['last_val'] = end($value);
// $out['last_lable'] = end($d);


?>