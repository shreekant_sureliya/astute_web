<?php 
$user_id = $_SESSION['user_data']['user_id'];
$device = Device::where('user_id',$user_id)->get();

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$no = get_form_value('no');
$i=1;
foreach($device as $d){
$serviceAccount = ServiceAccount::fromJsonFile($main_root_path."/firebase_json/".$d->json_file);
$firebase = (new Factory)
   ->withServiceAccount($serviceAccount)
   ->withDatabaseUri($d->url)
   ->create();
$database = $firebase->getDatabase();
//$data=$database->getReference('data')->getSnapshot();

$data=$database->getReference('data')
    ->orderByChild('data')
    ->limitToLast($no)
    ->getSnapshot();

// $updates = [
//   'XYZ' => '123',
//   'ABC' => '321',
// ];

// $database->getReference('data') // this is the root reference
//  ->update($updates);

$data=$data->getValue();
$xvalues=[];
$yvalues_I=[];
$yvalues_V=[];
date_default_timezone_set("Asia/Kolkata");
foreach($data as $key=>$val){
  $xvalues[] = date('d/m/Y h:i:s', $key);
  $yvalues_I[] = $val['I'];
  $yvalues_V[] = $val['V'];

}

$out['type'] = 'success';
$out['xvalue'][$i] = $xvalues;
$out['yvalues_I'][$i] = $yvalues_I;
$out['yvalues_V'][$i] = $yvalues_V;
$i++;
}



// $d=[];
// $value=[];
// foreach($data as  $key => $year){
// $yer=$key;
// foreach($year as  $key => $month){
//   $mon=$key;
//   foreach($month as $key=>$day){
//     $dy=$key;
//     foreach($day as $key=>$hour){
//       $h=$key;
//       foreach($hour as $key=>$minute){
//         $m=$key;
//         foreach($minute as $key=>$second){
//           $s=$key;
//           $d[]=$yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s;
//           //$d[] = strtotime($yer.'/'.$mon.'/'.$dy.' '.$h.':'.$m.':'.$s);
//           $value[]=$second;
//         }
//       }
//     }
//   }
// }
// }

// $out['type'] = 'success';
// $out['xvalue'] = array_slice($d,-9,9);
// $out['yvalue'] = array_slice($value,-9,9);
// $out['max'] = max($value);
// $out['min'] = min($value);
// $out['last_val'] = end($value);
// $out['last_lable'] = end($d);


?>