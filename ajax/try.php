<?php 


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$user_id = $_SESSION['user_data']['user_id'];
$device_id = get_form_value('id');
$date = get_form_value('date');

$date = explode('-',$date);

date_default_timezone_set("Asia/Kolkata");
$f = str_replace('/', '-', $date[0]);
$from_date = date('Y-m-d', strtotime($f));
$dtf   = new DateTime($from_date);

$t = str_replace('/', '-', $date[1]);
$to_date = date('Y-m-d', strtotime($t));
$dtt   = new DateTime($to_date);

$from=$dtf->getTimestamp();
$to=$dtt->getTimestamp();

//dd($from.'--------------'.$to);

$device = Device::where('user_id',$user_id)->where('id',$device_id)->first();
$serviceAccount = ServiceAccount::fromJsonFile($main_root_path."/firebase_json/".$device->json_file);
$firebase = (new Factory)
   ->withServiceAccount($serviceAccount)
   ->withDatabaseUri($device->url)
   ->create();
$database = $firebase->getDatabase();

$data=$database->getReference('data')
    ->orderByKey()
    ->getSnapshot();

$data=$data->getValue();
$k = [];
foreach($data as $key=>$data){
  if($key >= '164832454227' && $key<='164832454234'){
    $k[]=$data;
  }
}

$j='';
$i=0;
ob_start();

echo '<thead class="cf">
    <tr>
        <th>No</th>
        <th>Device Name</th>';
        foreach($k as $data){
          foreach($data as $d){
            '<th>'.$d.'</th>';
          }
        }
        echo '</tr>
  </thead>
  <tbody>';
  foreach($k as $dj){
    echo '<tr>
      <td>'.$i.'</td>
      <td>Name</td>';
      foreach($dj as $key=>$h){
       '<td>'.$h.'</td>';
      }     
  '</tr>';
  $i++; } 
  echo '</tbody>';

  $table_row = ob_get_contents();
  $table_row = str_replace("\n", '', $table_row);
  ob_clean();
  
  $out['type']      = 'success';
  $out['table_row'] = $table_row;
  dd($table_row);
  echo json_encode($out);
  die;

?>