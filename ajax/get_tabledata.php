<?php 


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$user_id = $_SESSION['user_data']['user_id'];
$device_id = get_form_value('id');
$date = get_form_value('date');

$date = explode('-',$date);

date_default_timezone_set("Asia/Kolkata");
$f = str_replace('/', '-', $date[0]);
$from_date = date('Y-m-d', strtotime($f));
$dtf   = new DateTime($from_date);

$t = str_replace('/', '-', $date[1]);
$to_date = date('Y-m-d', strtotime($t));
$dtt   = new DateTime($to_date);

$from=$dtf->getTimestamp();
$to=$dtt->getTimestamp();

//dd($from.'--------------'.$to);

$device = Device::where('user_id',$user_id)->where('id',$device_id)->first();
$serviceAccount = ServiceAccount::fromJsonFile($main_root_path."/firebase_json/".$device->json_file);
$firebase = (new Factory)
   ->withServiceAccount($serviceAccount)
   ->withDatabaseUri($device->url)
   ->create();
$database = $firebase->getDatabase();

$data=$database->getReference('data')
    ->orderByKey()
    ->getSnapshot();

$data=$data->getValue();
$k = [];

foreach($data as $key=>$data){
  if($key > $from && $key <= $to){
    $data['date']=date('H:i:s | d-m-Y',$key);
    //$data['date']=$key;
    $k[]=$data;
  }
}

$j='';
$i=1;

ob_start();

echo '<thead class="cf text-center">
    <tr>
        <th>No</th>';
        foreach($k as $data){
          foreach($data as $key=>$d){
            echo '<th>'.$key.'</th>';
          }
          break;
        }
        echo '</tr>
  </thead>
  <tbody class="text-center">';
  foreach($k as $key=>$dj){
    echo '<tr>
      <td>'.$i.'</td>';
      foreach($dj as $key=>$h){
       echo '<td>'.$h.'</td>';
      }     
  '</tr>';
  $i++; } 
  echo '</tbody>';

  $table_row = ob_get_contents();
  $table_row = str_replace("\n", '', $table_row);
  ob_clean();
  
  $out['type']      = 'success';
  $out['table_row'] = $table_row;
  echo json_encode($out);
  die;

?>