<?php
$db_mode = true;
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('devices', function ($table) {
	$table->increments('id');
    $table->string('device_id');
    $table->integer('user_id');
    $table->string('url');
    $table->string('json_file');
    $table->string('gmail');
    $table->string('password');
    $table->text('sensor');
    $table->string('last_update')->nullable();
    $table->integer('is_active')->default(1);
    $table->timestamps();
});
?>