<?php
$db_mode = true;
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table) {
	$table->increments('user_id');
    $table->string('user_name');
    $table->string('email')->unique();
    $table->string('password');
    $table->string('contact_no');
    $table->string('user_type');
    $table->integer('is_active')->default(1);
    $table->timestamps();
});
?>