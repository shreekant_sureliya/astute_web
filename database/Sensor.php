

<?php
$db_mode = true;
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('sensors', function ($table) {
	$table->increments('id');
    $table->string('name');
    $table->integer('is_active')->default(1);
    $table->timestamps();
});
?>