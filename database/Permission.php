<?php
$db_mode = true;
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('permission', function ($table) {
	$table->increments('id');
    $table->integer('user_id');
	$table->string('permission');
   	$table->integer('is_active')->default(1);
    $table->timestamps();
});
?>