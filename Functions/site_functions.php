<?php

function getvar($var){
		return ($var != NULL) ? $var : '';
}

function clean($string)
{
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function redirect($url,$full_url = ''){
	global $config;
	if($full_url == ''){
		header('Location:'.$config['site_url'].$url);
	}else{
		header('Location:'.$url);
	
	}
	die;
}

function get_form_value($name){

		if(isset($_REQUEST[$name]) && $_REQUEST[$name] != ''){
			return $_REQUEST[$name];
		}else{
			return NULL;
		}

}

function checkPermition($permition){
	$user_id = $_SESSION['user_data']['user_id'];
	$check_perm = Permission::where(array('user_id'=>$user_id,'permission'=>$permition,'is_active'=>1))->first();
	if($check_perm == NULL){
		return false;
	}else{
		return true;
	}
}

function searchForId($id, $array, $keys) {
   foreach ($array as $key => $val) {
	   if ($val[$keys] == $id) {
		   return $key;
	   }
   }
   return 'add';
}
function reArrayFiles(&$file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);
    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}
function getLast24Month() {
  for ($i = 0; $i < 12; $i++) {
		$months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
	}
  return array_reverse($months);
}
function getColor($k) {
	$color = array();
	$color[0] = array('r'=>60,'g'=>141,'b'=>188);
	$color[1] = array('r'=>210,'g'=>214,'b'=>222);
	$color[2] = array('r'=>245,'g'=>105,'b'=>84);
	$color[3] = array('r'=>0,'g'=>166,'b'=>90);
	$color[4] = array('r'=>0,'g'=>192,'b'=>239);
	$color[5] = array('r'=>145,'g'=>135,'b'=>31);
	
	if (array_key_exists($k, $color)){
  	$final_rgbColor = $color[$k];
  }else{
  	$rgbColor = array();
		foreach(array('r', 'g', 'b') as $color){
			$rgbColor[$color] = mt_rand(0, 255);
		}
		$final_rgbColor = $rgbColor;
  }
	return $final_rgbColor;
}
?>